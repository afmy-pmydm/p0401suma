package net.iescierva.dam20_18.p0401suma;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void addTwoNumbers(View view) {
        EditText n1EditText = findViewById(R.id.n1EditText);
        EditText n2EditText = findViewById(R.id.n2EditText);
        TextView resultTextView = findViewById(R.id.resultTextView);

        try {
            double n1 = Double.parseDouble(n1EditText.getText().toString());
            double n2 = Double.parseDouble(n2EditText.getText().toString());
            Locale locale = Locale.getDefault();
            resultTextView.setText(String.format(locale, "%10.2f", (n1 + n2)));
        } catch (Exception e) {
            resultTextView.setText(R.string.error_text);
        }
    }

}